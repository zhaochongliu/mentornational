$(document).ready(function(){
  $('form').on('submit', function(event){
    event.preventDefault();
    var title = $("#title");
    var cover = $("#cover");
    var date = $("#date");
    var abstract = $("#abstract");
    var html  = $("#html");
    var style = $("#style");

    var info = {abstract:abstract.val(), date:date.val(),
                title:title.val(), cover:cover.val(),
                html:html.val(), style:style.val()};

    console.log(info);

    $.ajax({
      type:'POST',
      url:'/admin/addArticle/add',
      data:info,
      success:function(data){
        console.log(data);
      }
    });
  })
})
