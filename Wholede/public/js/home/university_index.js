$(document).ready(function(){
  if ($("#lang").text() == "中"){
    $('.ontop-en').css('display','none');
    $('.assur-en').css('display','none');
    $('.ontop').css('display','block');
    $('.assur').css('display','block');
  }else {
    $('.ontop-en').css('display','block');
    $('.assur-en').css('display','block');
    $('.ontop').css('display','none');
    $('.assur').css('display','none');
  }
  $('.assesForm .alert').hide();
  $('form').on('submit', function(event){
    event.preventDefault();
    var name = $('#name');
    var school = $('#school');
    var major = $('#major');
    var grade = $('#grade');
    var email = $('#email');
    var cellphone = $('#cellphone');
    var target = $('#target');
    var gpa = $('#gpa');
    var info = {name:name.val(), school:school.val(), major:major.val(),
                grade:grade.val(), email:email.val(), cellphone:cellphone.val(),
                target:target.val(), gpa:gpa.val()};

    $.ajax({
      type:'POST',
      url:'/home/university/assesment',
      data:info,
      success:function(data){
        console.log(data);
        $('.assesForm .alert').show();
        $('.assesForm .alert').addClass('show');
        $('.assesForm .alert').addClass('fade');
      }
    });
  });
});
