$(document).ready(function(){

  if ($("#lang").text() == "中"){
    $('.en').css('top','35%');
    $('.en2').css('margin-top', '4%');
    $('.en3').css('margin-top', '4%');
    $('.24zh').css('display', 'block');
    $('.24en').css('display', 'none');
    $('.enp').css('margin-left','none');
  }else {
    $('.en').css('top','27%');
    $('.en2').css('margin-top', '3%');
    $('.en3').css('margin-top', '1.5%');
    $('.24zh').css('display', 'none');
    $('.24en').css('display', 'block');
    $('.enp').css('margin-left','-4vw')
  }

  $("#life").on("click", function(){
    $("#lawContent").collapse('hide');
    $("#careerContent").collapse('hide');
  });
  $("#law").on("click", function(){
    $("#lifeContent").collapse('hide');
    $("#careerContent").collapse('hide');
  });
  $("#career").on("click", function(){
    $("#lawContent").collapse('hide');
    $("#lifeContent").collapse('hide');
  });

  $("#f1img").hover(function(){$(this).css('opacity',0.5); $('#f1h').css('display','block')},
                    function(){$(this).css('opacity',1); $('#f1h').css('display','none')});
  $("#f1h").hover(function(){$("#f1img").css('opacity',0.5); $(this).css('display','block')},
                  function(){$("#f1img").css('opacity',1); $(this).css('display','none')}     );

  $("#criminalimg").hover(function(){$(this).css('opacity',0.5); $('#criminalh').css('display','block')},
                    function(){$(this).css('opacity',1); $('#criminalh').css('display','none')});
  $("#criminalh").hover(function(){$("#criminalimg").css('opacity',0.5); $(this).css('display','block')},
                  function(){$("#criminalimg").css('opacity',1); $(this).css('display','none')}     );

  $("#optimg").hover(function(){$(this).css('opacity',0.5); $('#opth').css('display','block')},
                    function(){$(this).css('opacity',1); $('#opth').css('display','none')});
  $("#opth").hover(function(){$("#optimg").css('opacity',0.5); $(this).css('display','block')},
                  function(){$("#optimg").css('opacity',1); $(this).css('display','none')}     );

  $("#backimg").hover(function(){$(this).css('opacity',0.5); $('#backh').css('display','block')},
                    function(){$(this).css('opacity',1); $('#backh').css('display','none')});
  $("#backh").hover(function(){$("#backimg").css('opacity',0.5); $(this).css('display','block')},
                  function(){$("#backimg").css('opacity',1); $(this).css('display','none')}     );
})
