$(document).ready(function(){

  $(window).scroll(function(){
    if($(window).scrollTop() > 30){
      $("#navbar").addClass("fixed-top");
      $("#navbar").addClass("animated");
      $("#navbar").addClass("fadeInDown");
    } else {
      $("#navbar").removeClass("fixed-top");
      $("#navbar").removeClass("animated");
      $("#navbar").removeClass("fadeInDown");
    }
  });

$("#burger").on('click', function(){
  $(this).toggleClass("open");
})

  if ($("#lang").text() == "中"){
    $('.introduction-en').css('display','none');
    $('.introduction').css('display','block');
  }else {
    $('.introduction-en').css('display','block');
    $('.introduction').css('display','none');
  }

  $("#fixedwechat").hover(function(){
    $("#QR").css("display","inline");
  }, function(){
    $("#QR").css("display","none");
  });

  $("#langsetzh").on("click", function() {
    var langSet = "zh";
    var setting = {lang: langSet};
    $.ajax({
      type:"POST",
      url:"/home/index",
      data:setting,
      success:function(data) {
        location.reload();
      }
    });
  });
  $("#langseten").on("click", function() {
    var langSet = "en";
    var setting = {lang: langSet};
    $.ajax({
      type:"POST",
      url:"/home/index",
      data:setting,
      success:function(data) {
        location.reload();
      }
    });
  });
  // if($(document).hasClass())
})
