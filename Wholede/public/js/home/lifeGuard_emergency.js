$(document).ready(function(){
  if ($("#lang").text() == "中"){
    $('.en').css('top','40%');
    $('.24zh').css('display', 'block');
    $('.24en').css('display', 'none');
    $('.enp').css('margin-left','none');
  }else {
    $('.en').css('top','30%');
    $('.24zh').css('display', 'none');
    $('.24en').css('display', 'block');
    $('.enp').css('margin-left','-4.5vw');
  }
})
