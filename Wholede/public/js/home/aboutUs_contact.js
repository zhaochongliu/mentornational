$(document).ready(function(){
  if ($("#lang").text() == "中"){
    $('.ontop-en').css('display','none');
    $('.ontop').css('display','block');
  }else {
    $('.ontop-en').css('display','block');
    $('.ontop').css('display','none');
  }
  $('.message .alert').hide();
  $('form').on('submit', function(event){
    event.preventDefault();
    var name = $('#yourName');
    var phone = $('#yourPhone');
    var email = $('#yourEmail');
    var program = $('#yourProgram');
    var request = $('#yourRequest');
    var info = {name:name.val(), phone:phone.val(),
                email:email.val(), program:program.val(),
                request:request.val()};

    $.ajax({
      type:'POST',
      url:'/home/aboutUs/message',
      data:info,
      success:function(data){
        console.log(data);
        $('.message .alert').show();
        $('.message .alert').addClass('show');
        $('.message .alert').addClass('fade');
      }
    });
  });
});
