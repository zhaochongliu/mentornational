var express = require("express");
var homeIndex = require("./controllers/home/index");
var adminIndex = require("./controllers/admin/adminIndex")
//start app
var app = express();

//set the veiw engine
app.set("view engine", "ejs");

//set the static file
app.use("/static", express.static(__dirname + "/public"));
app.use("/parts", express.static(__dirname + "/views/home"));

//fire controllers
homeIndex(app);
adminIndex(app);
//set web port
app.listen(3000);
console.log("now listen to the port 3000");
