var bodyParser = require("body-parser");

var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports = function(app) {

  app.get("/home/study/index",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/study_index", {language: language});
  });

  app.get("/home/study/language",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/study_language", {language: language});
  });

  app.get("/home/study/major",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/study_major", {language: language});
  });

  app.get("/home/study/alltheway",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/study_alltheway", {language: language});
  });

  app.get("/home/study/activity", function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/study_activity", {language: language});
  })
}
