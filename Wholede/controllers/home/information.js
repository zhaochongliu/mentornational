var bodyParser = require("body-parser");
var dbSchema = require("../../modles/dbschemas");

var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports = function(app) {

  app.get("/home/information/index",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";

    dbSchema.Article.find({},null, {sort:'date'}, function(err, articles){
      if(err) throw err;
      // articles.forEach(function(article){
      //   articleMap.set(article.cover, article.title);
      // })
      // console.log(articleMap.size);
      res.render("home/information_index", {language: language, articles: articles});
    })
  });

  app.get("/home/information/ranks",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";

    // var newrank = RankTest({school:'123', rank:1}).save(function(err, data){
    //   console.log(data);
    // });
    dbSchema.RankTest.find({}, function(err, data){
      if(err) throw err;
      console.log(data);
      res.render("home/information_ranks", {language: language, ranklist: data});
    });
  });

  app.get("/home/information/articles", urlencodedParser, function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/information_articles", {language: language});
  });

  app.get("/home/information/articles/:id", urlencodedParser, function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    var id = req.params.id;
    dbSchema.Article.findById(id, function(err, article){
      if(err) throw err;
      //use id to find the article and wrap the article into json
      res.render("home/information_articles", {language: language, article:article});
    })
  });


  app.get("/home/information/activities",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/information_activities", {language: language});
  });
}
