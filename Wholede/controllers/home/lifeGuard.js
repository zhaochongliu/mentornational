var bodyParser = require("body-parser");

var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports = function(app) {

  app.get("/home/lifeguard/index",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/lifeGuard_index", {language: language});
  });

  app.get("/home/lifeguard/attorney",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/lifeGuard_attorney", {language: language});
  });

  app.get("/home/lifeguard/cuddling",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/lifeGuard_cuddling", {language: language});
  });

  app.get("/home/lifeguard/emergency",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/lifeGuard_emergency", {language: language});
  });

  app.get("/home/lifeguard/landing",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/lifeGuard_landing", {language: language});
  });
}
