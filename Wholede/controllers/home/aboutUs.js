var bodyParser = require("body-parser");
var nodemailer = require("nodemailer");

var urlencodedParser = bodyParser.urlencoded({extended: false});
// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'wholede.user@gmail.com',
        pass: 'passwordWholde'
    }
});

module.exports = function(app) {

  app.get("/home/aboutUs/index",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/aboutUs_index", {language: language});
  });


  app.get("/home/aboutUs/contact",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/aboutUs_contact", {language: language});
  });

  app.post('/home/aboutUs/message', urlencodedParser, function(req, res){
    var newdata = req.body;
    var mailOptions = {
        from: '"Wholede" <Wholede.user@gmail.com>', // sender address
        to: 'info@wholede.org', // list of receivers
        subject: 'Contact Request', // Subject line
        text: JSON.stringify(newdata) // plain text body
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
    res.send('success');
  })
}
