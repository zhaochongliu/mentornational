var bodyParser = require("body-parser");

var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports = function(app) {

  app.get("/home/ustrip/index",function(req, res){
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/ustrip_index", {language: language});
  });
}
