var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var i18n = require("i18n");

var universityIndex = require("./university");
var highSchoolIndex = require("./highSchool");
var lifeGuardIndex = require("./lifeGuard");
var aboutUsIndex = require("./aboutUs");
var ustripIndex = require("./ustrip");
var studyIndex = require("./study");
var informationIndex = require("./information");
var urlencodedParser = bodyParser.urlencoded({extended: false});

i18n.configure({
  // setup some locales - other locales default to en silently
  locales: ['en', 'zh'],
  // you may alter a site wide default locale
  defaultLocale: 'zh',
  cookie: 'glang',
  queryParameter: 'lang',
  // where to store json files - defaults to './locales'
  directory: __dirname + '/Locales'
});
// console.log(i18n.__('Hello'));
module.exports = function(app) {

  app.use(cookieParser());
  app.use(i18n.init);

  highSchoolIndex(app);
  universityIndex(app);
  lifeGuardIndex(app);
  aboutUsIndex(app);
  ustripIndex(app);
  informationIndex(app);
  studyIndex(app);

  app.get("/", function(req, res) {
    res.redirect("/home/index");
  })

  app.get("/home/index", function(req, res) {
    var glang = req.cookies.glang;
    if(glang == null) {
      res.cookie("glang", "en");
      glang = "en";
    }
    var language = "ENG";
    if (glang === "zh") language = "中";
    res.render("home/index", {language: language});
    });

  app.post("/home/index", urlencodedParser, function(req, res) {
    var langSet = req.body.lang;
    // console.log(langSet);
    res.cookie("glang", langSet);

    // console.log(res.__('hello'));
    res.end();
  });
}
