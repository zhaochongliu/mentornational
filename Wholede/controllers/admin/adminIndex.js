var bodyParser = require("body-parser");
var addArticle = require("./addArticle");

var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports = function(app){

  app.get("/admin/index", function(req, res){
    res.render("admin/index");
  });

  addArticle(app);
}
