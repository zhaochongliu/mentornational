var bodyParser = require("body-parser");
var dbSchema = require("../../modles/dbschemas");

var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports = function(app){

  app.get("/admin/addArticle", function(req, res){
    res.render("admin/addArticle");
  });

  app.post("/admin/addArticle/add", urlencodedParser, function(req, res){
    data = req.body;
    console.log(data);
    var newdata = dbSchema.Article(data).save(function(err, data){});
    res.send('success');
  });

  app.get("/admin/article/preview", function(req, res){
    res.render("home/information_articles_preview");
  });
}
