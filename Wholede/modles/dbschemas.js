var mongoose = require("mongoose");
mongoose.connect('mongodb://wholede:wholede.com@ds149030.mlab.com:49030/wholede');

var rankSchema = new mongoose.Schema({
  school: String,
  rank: Number,
});


var articleSchema = new mongoose.Schema({
  title:String,
  cover:String,
  abstract:String,
  date:Date,
  html:String,
  style:String
});
var RankTest = mongoose.model('RankTest', rankSchema);
var Article = mongoose.model('Article', articleSchema);
module.exports = {
    RandTest: RankTest,
    Article: Article
}
